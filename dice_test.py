import unittest
import random

import dice

class TestDice(unittest.TestCase):

    def test_roll(self):
        random.seed(5)
        self.assertEqual(dice.roll('1d6'), [5])
        self.assertEqual(dice.roll('4d6'), [3, 6, 3, 6])
        self.assertEqual(dice.roll('1d8'), [1])
        self.assertEqual(dice.roll('4d8'), [8, 4, 1, 3])
        self.assertEqual(dice.roll('4d20'), [4, 12, 16, 8])
        self.assertEqual(dice.roll('10d4'), [4, 1, 2, 1, 2, 4, 3, 2, 4, 2])
        self.assertEqual(dice.roll('1d100'), [98])

    def test_roll_total(self):
        random.seed(5)
        self.assertEqual(dice.roll_total('1d6'), 5)
        self.assertEqual(dice.roll_total('4d6'), 18)
        self.assertEqual(dice.roll_total('1d8'), 1)
        self.assertEqual(dice.roll_total('4d8'), 16)
        self.assertEqual(dice.roll_total('4d20'), 40)
        self.assertEqual(dice.roll_total('10d4'), 25)
        self.assertEqual(dice.roll_total('1d100'), 98)

    def test_get_dice(self):
        random.seed(5)
        self.assertEqual(dice.get_dice(4), 3)
        self.assertEqual(dice.get_dice(6), 6)
        self.assertEqual(dice.get_dice(8), 6)
        self.assertEqual(dice.get_dice(12), 12)
        self.assertEqual(dice.get_dice(20), 17)
        self.assertEqual(dice.get_dice(100), 4)


if __name__ == '__main__':
    unittest.main()